# Introdução

## Equipe

O projeto foi desenvolvido pelos alunos de Engenharia de Computação - Introdução a Engenharia. 

|Nome| gitlab user|
|---|---|
|-|-|
|-|-|
|-|-|

# Documentação

A documentação do projeto pode ser acessada pelo link:

https://gladsonandrade.gitlab.io/mypage 

# Links Úteis

* [Tutorial HTML](http://pt-br.html.net/tutorials/html/)
* [Gnuplot](http://fiscomp.if.ufrgs.br/index.php/Gnuplot)